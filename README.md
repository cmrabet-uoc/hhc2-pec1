# HTML and CSS Tools II

## Project 1 - Universitat Oberta de Catalunya

[Chakir Mrabet](mailto:cmrabet@gmail.com) - October 30, 2020,


# What is it?

This is a web-based single-page resume implemented using Parcel, SASS, Stylelint, and the [H.Roberts style user guide](https://cssguidelin.es/).

# Where is it?

You can visit the site [here](https://cmrabet-hhc2-uoc.netlify.app/).

# Instructions

For development, run the following command from the root folder:

`npm run dev`

For production:

`npm run build`

The files for production will be in the `dist` folder.