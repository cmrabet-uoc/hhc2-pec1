/**
 * Herramientas HTML y CSS II - PEC 1.
 * Chakir Mrabet, 2020.
 * Entry script.
 */

// This is to fix the issue explained: https://flaviocopes.com/parcel-regeneratorruntime-not-defined/
// This is an issue with Parcel.
import "regenerator-runtime/runtime";

/**
 * Custom made modules.
 */
import nav from "./nav";
import menu from "./menu";
import contact from "./contact";

/**
 * Main nav bar functionality.
 */

nav();

/**
 * Main menu functionality.
 */

menu();

/**
 * Contact form functionality.
 */

contact();
