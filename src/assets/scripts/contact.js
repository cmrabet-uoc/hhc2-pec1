/**
 * Contact Form Functionality.
 *
 * Submits the values of the form to the 3rd party service
 * FormSpree using Fetch.
 *
 * 1. While submitting, it hides the container that has the form
 *    and contact information. It also shows a loader.
 * 2. When the submission is finished, it shows a thank you message
 *    for 5 seconds before showing again the container that has the
 *    form and contact information.
 * 3. If there is a problem with the submission, an alert box will
 *    inform the user.
 */

const FORM_API = "https://formspree.io/f/xoqpwjzz";

export default function test() {
  const formLoaderEl = document.querySelector(".js-contact-form-loader");
  const thanksTextEl = document.querySelector(".js-contact-form-thanks");
  const formContainerEl = document.querySelector(".js-contact-form-container");
  const formEl = document.querySelector(".js-contact-form");

  if (formEl) {
    formEl.addEventListener("submit", async (e) => {
      e.preventDefault();

      show(formContainerEl, false);
      show(formLoaderEl, true);

      const body = new FormData(formEl);

      try {
        const res = await fetch(FORM_API, {
          method: "post",
          headers: {
            Accept: "application/json",
          },
          body,
        });

        if (res.ok) {
          formEl.reset();

          show(formLoaderEl, false);
          show(thanksTextEl, true);

          setTimeout(() => {
            show(thanksTextEl, false);
            show(formContainerEl, true);
          }, 5000);
          return;
        }

        showError(`Your request was not submitted. ${res.statusText}`);
      } catch (e) {
        showError(
          "There was an error when submitting your request, please try again later."
        );
      }

      show(thanksTextEl, false);
      show(formContainerEl, true);
    });
  }
}

function show(el, isVisible) {
  if (isVisible) {
    el.classList.remove("l-hidden");
    el.classList.add("l-visible");
    return;
  }

  el.classList.remove("l-visible");
  el.classList.add("l-hidden");
}

function showError(msg) {
  alert(msg);
}
