/**
 * Nav bar functionality.
 *
 * 1. Controls when the bar is static or becomes sticky when the user
 *    passes the fold section.
 * 2. Detects the section in which the user is currently scrolling in,
 *    and sets the correspondent nav menu anchor as the active one.
 */

export default function nav() {
  const navEl = document.querySelector(".js-nav");
  const mainEl = document.querySelector("main");
  const linksEl = document.querySelectorAll(".js-nav [href]");
  const foldHeight = window.innerHeight;

  // Control the nav static/sticky behavior.

  if (navEl) {
    window.addEventListener("scroll", () => {
      updateLinks();

      if (window.scrollY > foldHeight) {
        navEl.classList.add("nav--is-fixed");
        mainEl.classList.add("nav__sibling--is-fixed");
        return;
      }

      navEl.classList.remove("nav--is-fixed");
      mainEl.classList.remove("nav__sibling--is-fixed");
    });
  }

  // Finds the current selected anchor and sets its class.

  function updateLinks() {
    const navElHeight = navEl.clientHeight;
    const sections = document.querySelectorAll("section");

    linksEl.forEach((linkEl) => linkEl.classList.remove("link--is-selected"));

    sections.forEach((sectionEl) => {
      if (
        window.scrollY > sectionEl.offsetTop + navElHeight &&
        window.scrollY <
          sectionEl.offsetTop + sectionEl.clientHeight + navElHeight
      ) {
        const linkEl = document.querySelector(
          `.js-nav [href="#${sectionEl.id}"]`
        );
        linkEl.classList.add("link--is-selected");
      }
    });
  }
}
