/**
 * Main Menu Functionality.
 *
 * Adds a listener to the menu toggle button and sets the proper
 * classes to the menu elements to show the version for small
 * screens. The menu toggle button only shows in small screens,
 * determined from CSS.
 */

export default function initMenu() {
  const menuButton = document.querySelector(".js-menu__toggle-button");
  const menuOptions = document.querySelector(".js-menu__options");

  if (menuButton) {
    menuButton.addEventListener("click", (e) => {
      e.preventDefault();

      menuButton.classList.toggle("menu__toggle-button--is-clicked");
      menuOptions.classList.toggle("menu__options--is-expanded");
    });
  }
}
