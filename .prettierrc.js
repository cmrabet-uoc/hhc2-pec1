module.exports = {
  tabWidth: 2,
  printWidth: 80,
  overrides: [
    {
      files: ["*.html"],
      printWidth: 110,
    },
  ],
};
